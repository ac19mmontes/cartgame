package com.example.cartas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private ArrayList<carta> listacartas;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CartaAdapter cartaAdapter;
    public int[] llistaimatges={R.drawable.c0,R.drawable.c1,R.drawable.c2,R.drawable.c3,
            R.drawable.c4,R.drawable.c5,R.drawable.c6,R.drawable.c7,R.drawable.c8,R.drawable.c9,R.drawable.c10,R.drawable.c11};
    public int NUMERO_COLUMNAS =4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        seleccionarCartes() ;
        recyclerView= findViewById(R.id.reciclerAunt);
        layoutManager= new GridLayoutManager(this, NUMERO_COLUMNAS);
        cartaAdapter = new CartaAdapter(this, listacartas);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(cartaAdapter);

    }
    public void seleccionarCartes() {
        listacartas = new ArrayList();
        for (int i=0; i<llistaimatges.length; i++)
        {
            listacartas.add(new carta(llistaimatges[i]));
        }
        Collections.shuffle(listacartas);
        for (int i=listacartas.size()/2; i<listacartas.size(); i++)
        {
            listacartas.get(i).setFrontimage(listacartas.get(i-listacartas.size()/2).getFrontimage());
        }
        Collections.shuffle(listacartas);
    }
}