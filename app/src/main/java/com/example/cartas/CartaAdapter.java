package com.example.cartas;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class CartaAdapter extends RecyclerView.Adapter<CartaAdapter.ImageHolder> {

    private int turnos, carta1, carta2, pos1, pos2;
    private Context context;
    private ArrayList<carta> listaCarta;

    public CartaAdapter(Context context, ArrayList<carta> listaCarta) {
        this.context = context;
        this.listaCarta = listaCarta;
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.main2, parent, false);
        return new ImageHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHolder holder, int position) {
        holder.imageView.setImageResource(listaCarta.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return listaCarta.size();
    }

    public class ImageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public ImageView imageView;

        public ImageHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            imageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            TextView textView;


            if (true) {


                if (turnos == 0) {
                    listaCarta.get(getAdapterPosition()).girar();
                    carta1 = listaCarta.get(getAdapterPosition()).getImage();
                    pos1 = getAdapterPosition();
                    turnos++;
                    notifyDataSetChanged();
                } else if (turnos != 2) {
                    listaCarta.get(getAdapterPosition()).girar();
                    turnos++;
                    carta2 = listaCarta.get(getAdapterPosition()).getImage();

                    pos2 = getAdapterPosition();


                    if (carta1 == carta2) {

                        listaCarta.get(pos1).setEstat(carta.Estat.FIXED);
                        listaCarta.get(pos2).setEstat(carta.Estat.FIXED);
                        System.out.println("son iguales");
                        notifyDataSetChanged();
                        turnos = 0;
                    } else {


                        System.out.println("son distintas");

                        final Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                listaCarta.get(pos1).setEstat(carta.Estat.BACK);
                                listaCarta.get(pos2).setEstat(carta.Estat.BACK);
                                notifyDataSetChanged();
                                turnos = 0;

                            }
                        }, 1000);
                    }
                    notifyDataSetChanged();
                }

                System.out.println("turnos:" + turnos);


            }
        }
    }
}
